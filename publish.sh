#!/bin/bash

# Set user
git config user.name "Permission Photography"
git config user.email "permission.photography@gmail.com"

# Commit and push
git checkout master
git add .
git commit -m "Update"
git push
